// React Native SVG Component
// https://github.com/react-native-community/react-native-svg
//
// Process
// 1. Clean up SVG in graphics app (Illustrator/Sketch/etc) to set consistent width, height, and make sure the viewBox is "0, 0, W, H"
// 2. Open SVG in text editor to remove width, height, and any undesired/unnecessary styles
// 3. Open in https://jakearchibald.github.io/svgomg/ and optimize.
// 4. Integrate info app by converting SVG tags to component-based SVG tags used in https://github.com/react-native-community/react-native-svg and updating with JS variables such that I can controls dimensions, colors, etc.
// 5. Use in other components.
//
// For a set of icons, place converted SVGs in a single component and pass a "name" property to return the desired SVG. See below.
//
// Usage:
// import SVG from '../Components/SVG'
// <SVG name='logo' height={100} fill='tomato' />

import React from 'react'
import _ from 'lodash'
import Svg, {
    Circle,
    G,
    Path
} from 'react-native-svg'

const SVG = ({ name, fill, width, height, ...otherProps }) => {
    const graphics = {
        logo: {
            width: 70,
            height: 68,
            content: <Path fill={fill} d='M65.3 9.2v12.2C60.1 9.2 48 .6 33.9.6 17.7.6 4.1 12 .7 27.2h4.1c3.3-13 15.1-22.6 29.1-22.6 12.6 0 23.3 7.8 27.8 18.8H51.1v4h18.2V9.2h-4zM36.1 63.4c-12.6 0-23.3-7.8-27.8-18.8h10.6v-4H.7v18.2h4V46.6C9.9 58.8 22 67.4 36.1 67.4c16.2 0 29.8-11.4 33.2-26.6h-4.1c-3.3 12.9-15.1 22.6-29.1 22.6z' />
        },
        location: {
            width: 512,
            height: 512,
            content:
                <G fill={fill}>
                    <Path d='M255.9 0c-84.18 0-152.56 70.54-152.56 157.13 0 18 7.86 38.9 19 63.68s26.05 52.81 41.9 80.72c31.7 55.81 67.17 112 80.95 138.72a12 12 0 0 0 21.52 0c13.81-26.69 49.24-82.9 80.95-138.72 15.85-27.91 30.71-55.94 41.9-80.72s19-45.65 19-63.68C408.66 70.54 340.14 0 255.9 0zm0 77c40.78 0 73.71 33.69 73.71 75.63s-32.93 75.63-73.71 75.63-73.52-33.69-73.52-75.63S215.2 77 255.9 77z' />
                    <Path d='M107.15 379.16a12 12 0 0 0-9.15 4.12L3.16 491A12.82 12.82 0 0 0 1 504.5a12.18 12.18 0 0 0 11.11 7.5H499.7a12.17 12.17 0 0 0 11.24-7.41 12.82 12.82 0 0 0-2.1-13.59L414 383.28a12 12 0 0 0-9.14-4.11h-79.44a12.13 12.13 0 0 0-10.58 6.26 12.85 12.85 0 0 0 0 12.56 12.13 12.13 0 0 0 10.58 6.26h74.09l72.76 82.68H39.73l72.76-82.68h73.9A12.13 12.13 0 0 0 197 398a12.85 12.85 0 0 0 0-12.56 12.13 12.13 0 0 0-10.58-6.26h-79.27z' />
                </G>
        }
    };
    const viewBoxWidth = graphics[name].width;
    const viewBoxHeight = graphics[name].height;
    const viewBoxRatio = viewBoxWidth / viewBoxHeight;
    return (
        <Svg
            width={width || height && _.parseInt(height * viewBoxRatio) || 100}
            height={height || width && _.parseInt(width / viewBoxRatio) || 100}
            viewBox={`0 0 ${viewBoxWidth} ${viewBoxHeight}`}
            {...otherProps}
        >
            {graphics[name].content}
        </Svg>
    )
};

SVG.propTypes = {
    name: React.PropTypes.string.isRequired,
    fill: React.PropTypes.string,
    width: React.PropTypes.number,
    height: React.PropTypes.number
};

SVG.defaultProps = {
    name: 'logo',
    fill: 'black'
};

export default SVG