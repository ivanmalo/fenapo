/**
 * Created by Cesar on 05/07/2017.
 */
import React, { Component } from 'react';
import {
    AppRegistry,
    Dimensions,
    StyleSheet,
    Text,
    TouchableOpacity,
    StatusBar,
    View,
    NativeModules
} from 'react-native';
import Camera from 'react-native-camera';
import Entypo from 'react-native-vector-icons/Entypo';
import Icon from 'react-native-vector-icons/Ionicons';

//const myIcon = (<Ionicons name="flash" size={30} color="#900" />);

import { Actions } from 'react-native-router-flux';
var PESDK = NativeModules.PESDK;

export default class Camara extends React.Component {
    constructor(props) {
        super(props);

        this.camera = null;

        this.state = {
            camera: {
                aspect: Camera.constants.Aspect.fill,
                type: Camera.constants.Type.front,
                orientation: Camera.constants.Orientation.auto,
                flashMode: Camera.constants.FlashMode.auto,
            },
            isRecording: false
        };
    }

    switchType = () => {
        let newType;
        let newAspect;
        const { back, front } = Camera.constants.Type;
        const { fit, fill, stretch } = Camera.constants.Aspect;
        console.log('cambiamos tipo. Tipos, back: ' + back + '. Front: ' + front + '.');

        if (this.state.camera.type === back) {
            newType = front;
            newAspect = fill;
            //console.log('front');
        } else if (this.state.camera.type === front) {
            newType = back;
            newAspect = fit;
            //console.log('back');
        }
        console.log(this.state.camera.type);
        this.setState({
            camera: {
                ...this.state.camera,
                type: newType,
                aspect: newAspect,
            },
        });
    }

    takePicture = () => {
        const options = {};
        //options.location = ...
        this.camera.capture({metadata: options})
            .then((data) => {
                var   path = data.path;
                const imgPath = path.substring(7);
                //Actions.imagen({data: data})
                PESDK.present(imgPath);
            })
            .catch(err => console.error(err));
    }

    switchFlash = () => {
        let newFlashMode;
        const { auto, on, off } = Camera.constants.FlashMode;

        if (this.state.camera.flashMode === auto) {
            newFlashMode = on;
        } else if (this.state.camera.flashMode === on) {
            newFlashMode = off;
        } else if (this.state.camera.flashMode === off) {
            newFlashMode = auto;
        }
        console.log(this.state.camera.flashMode);
        this.setState({
            camera: {
                ...this.state.camera,
                flashMode: newFlashMode,
            },
        });
    }

    get flashIcon() {
        let icon;
        const { auto, on, off } = Camera.constants.FlashMode;

        if (this.state.camera.flashMode === auto) {
            icon = require('../assets/ic_flash_auto_white.png');
        } else if (this.state.camera.flashMode === on) {
            icon = require('../assets/ic_flash_on_white.png');
        } else if (this.state.camera.flashMode === off) {
            icon = require('../assets/ic_flash_off_white.png');
        }

        return icon;
    }

    render() {
        return (
            <View style={styles.container}>
                <StatusBar
                    animated
                    hidden
                />
                <Camera
                    ref={(cam) => {
                        this.camera = cam;
                    }}
                    type={this.state.camera.type}
                    style={styles.preview}
                    flashMode={this.state.camera.flashMode}
                    aspect={Camera.constants.Aspect.fill}>
                    <Text style={styles.capture} onPress={this.takePicture}>
                        <Entypo name="camera" size={30} />
                    </Text>
                </Camera>
                <View style={[styles.overlay, styles.topOverlay]}>
                    <Text style={styles.reverse}
                        onPress={this.switchType}
                    >
                        <Entypo
                            name = "ccw" size = {30} />
                    </Text>
                    <Text
                        style={styles.flash}
                        onPress={this.switchFlash}
                    >
                        <Entypo name = "flash" size = {30} />
                    </Text>
                </View>
                <View style = {styles.bottomOverlay}>
                    <Text style={styles.gallery} onPress={Actions.Galeria}>
                        <Entypo name="image" size={30} />
                    </Text>
                </View>
            </View>
        );
    }


}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
    },
    preview: {
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'center'
    },
    capture: {
        flex: 0,
        backgroundColor: '#fff',
        borderRadius: 40,
        color: '#000',
        padding: 15,
        margin: 40,
        opacity: 0.45,
    },
    overlay: {
        position: 'absolute',
        padding: 16,
        right: 0,
        left: 0,
        alignItems: 'center',
    },
    topOverlay: {
        top: 0,
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    flash: {
        flex: 0,
        backgroundColor: '#fff',
        borderRadius: 40,
        marginRight: 15,
        marginTop: 20,
        color: '#000',
        padding: 15,
        margin: 0,
        opacity: 0.45,
    },
    reverse: {
        flex: 0,
        backgroundColor: '#fff',
        borderRadius: 40,
        marginLeft: 15,
        marginTop: 20,
        color: '#000',
        padding: 15,
        margin: 0,
        opacity: 0.45,
    },
    bottomOverlay: {
        flexDirection: 'column',
        bottom: 0,
        right: 0,
        flex: 0,
        position: 'absolute',
        alignItems: 'center',
        justifyContent: 'center'
    },
    gallery: {
        flex: 0,
        backgroundColor: '#fff',
        borderRadius: 40,
        color: '#000',
        padding: 15,
        marginRight: 10,
        marginBottom: 40,
        opacity: 0.45,
    },
});