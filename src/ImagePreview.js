/**
 * Created by Cesar on 10/07/2017.
 */

const FBSDK = require('react-native-fbsdk');
import React, { Component } from 'react';
import {
    Image,
    Text,
    StyleSheet,
    View,
    TouchableOpacity,
} from 'react-native';
import Icon from 'react-native-vector-icons/Entypo';
//import { ShareDialog } from 'react-native-fbsdk';

const {
    ShareDialog,
} = FBSDK;

export default class ImagePreview extends Component {
        
        constructor(props) {
            super(props);
            const shareLinkContent = {
                contentType: 'link',
                contentUrl: "https://facebook.com",
                contentDescription: 'Facebook sharing is easy!',
            };
            const sharePhoto = {
                contentType: 'photo',
                photos: [{
                    imageUrl: props.path,
                    userGenerated: false,
                    caption: "Hello World!"
                }]
            };

            this.state = {
                shareLinkContent: shareLinkContent,
                sharePhoto: sharePhoto,
                imagenUri: props.path,
            };
        }


    clickClick(){
        console.log('click click');
        var tmp = this;
        ShareDialog.canShow(this.state.shareLinkContent).then(
            function(canShow) {
                if (canShow) {
                    return ShareDialog.show(tmp.state.sharePhoto);
                }
            }
        ).then(
            function(result) {
                if (result.isCancelled) {
                    alert('Share operation was cancelled');
                } else {
                    alert('Share was successful with postId: '
                        + result.postId);
                }
            },
            function(error) {
                errorSub = error.message.substring(0,35);
                console.log(errorSub);
                if(errorSub == 'Unable to show the provided content'){
                    alert('Para poder compartir imágenes es necesario tener la App de Facebook instalada.');
                }else{
                    alert('Share failed with error: ' + error.message);
                }
            }
        );
    }

    render() {
        return (
            <View style = {styles.mainContainer}>
                <Image
                    source={{uri: this.state.imagenUri, isStatic:false}}
                    style={ styles.imagen }
                >
                </Image>
                <View style={styles.overlay} >
                    <TouchableOpacity style={ styles.bottomOverlay}>
                        <Image
                            source={ require('../img/compartir.png')}
                            style={ styles.capture }
                        />

                    </TouchableOpacity>
                </View>

            </View>

        );
        //const base64image = this.props.data.mediaUri;
        //const base64image = 'file://'+'/storage/emulated/0/DCIM/fenapo/fenapo_1501128418820.jpg';
        //console.log(base64image);
        //source={{uri: base64image, isStatic:false}}
    }

}

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
    },
    anchoMenu: {
        flex: 1,
        flexDirection: 'row',
        marginRight: 10,
        marginLeft: 10,
        marginBottom: 20,
    },
    imagen: {
        flex: 9,
        justifyContent: 'flex-end',
        alignItems: 'center',
        zIndex: -5
    },
    capture: {
        flex: 0,
        margin: 40,
        zIndex: 5,
    },
    overlay: {
        flexDirection: 'column',
        bottom: 0,
        left: 0,
        right: 0,
        flex: 0,
        position: 'absolute',
        alignItems: 'center',
        justifyContent: 'center'
    },

});