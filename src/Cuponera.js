import React, { Component } from 'react';
import {ScrollView} from 'react-native';
import Cupones1 from './Cupones1';
import Cupones2 from './Cupones2';
import Cupones3 from './Cupones3';
import Cupones4 from './Cupones4';

export default class Camara extends Component {

	renderCupones(){
		var cupon = Math.floor(Math.random() * 4) + 1;  

		console.log(cupon);

		if(cupon == 1){
			return( <Cupones1 /> );
		}
		else if(cupon == 2) {
			return( <Cupones2 /> );
		}
		else if(cupon == 3) {
			return( <Cupones3 /> );
		}
		else{
			return( <Cupones4 /> );
		}
	}

	render(){
		return(
			<ScrollView style={styles.mainStyle}>
					{this.renderCupones()}	
			</ScrollView>
		);
	}

}

const styles = {
	mainStyle: {
		flex: 1,
		backgroundColor: '#4d7044',
	}
}