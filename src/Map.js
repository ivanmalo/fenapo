/**
 * Created by Cesar on 10/07/2017.
 */

import React, { Component } from 'react';
import {
    Image,
    StyleSheet,
    View
} from 'react-native';

export default class Map extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        const mapPath = '../img/mapa.jpg';
        return (
            <View style = {styles.mainContainer}>
                <Image
                    source={require ( '../img/MapaFENAPO.jpg' )}
                    style={ styles.imagen }
                >
                </Image>
            </View>

        );
    }

}

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        justifyContent: 'center',
        flexDirection: 'column',
    },
    anchoMenu: {
        flex: 1,
        flexDirection: 'row',
        marginRight: 10,
        marginLeft: 10,
        marginBottom: 20,
    },
    imagen: {
        flex: 1,
        width: null,
        height: null,
    },
    capture: {
        flex: 1,
        backgroundColor: '#fff',
        borderRadius: 40,
        color: '#000',
        padding: 15,
        margin: 40,
        opacity: 0.45,
        zIndex: 5,
    }
});