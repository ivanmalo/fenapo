import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableHighlight,
  Modal,
  StyleSheet,
  Button,
  Image,
  Dimensions,
  ScrollView,
  RefreshControl
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import RNFetchBlob from 'react-native-fetch-blob';

let styles
const { width } = Dimensions.get('window')

export default class Galeria extends Component {
	
	constructor(props) {
	    super(props);
	  	this.state = {
		    imgs:[]
		}
	}

	componentWillMount(){
		this.scanPhotos();
    }
	
	scanPhotos = () => {
	    const dirs = RNFetchBlob.fs.dirs;
	    var pathFenapo = dirs.DCIMDir+"/fenapo/";
	    this.setState({ path: "file://"+pathFenapo });
	    RNFetchBlob.fs.ls(pathFenapo)
	    .then((files) => {
	    	this.setState({ imgs: files });
	        files.map(file => {
	          RNFetchBlob.fs.scanFile([{ path : pathFenapo+file }]);
	        })
	    })
	    .catch((error)=>{
	       alert(error.message);
	    })
    }

    render(){
    	return(
    		<ScrollView contentContainerStyle={styles.scrollView}>
              {
                this.state.imgs.map((p,i) => {
                  return (
                    <TouchableHighlight onPress={ () => Actions.imagen({path: this.state.path+p}) } >
                      <Image style= {{ width: width/3, height: width/3 }}
                        source={{uri: this.state.path+p, isStatic:false}} />
                    </TouchableHighlight>
                  )
                })
              }
            </ScrollView>
    	);
    }
}


styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  modalContainer: {
    paddingTop: 20,
    flex: 1
  },
  scrollView: {
    flexWrap: 'wrap',
    flexDirection: 'row'
  },
  shareButton: {
    position: 'absolute',
    width,
    padding: 10,
    bottom: 0,
    left: 0
  }
})
