/**
 * Created by Cesar on 11/07/2017.
 */

import React, { Component } from 'react';
import {
    StyleSheet,
    View
} from 'react-native';
import SVG from './SVG';
import Firefox from '../img/svg/Firefox';

export default class pruebaSVG extends Component {
    render() {
        return (
            <View style = {styles.mainContainer}>
                <Firefox/>
            </View>

        );
    }
}

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        justifyContent: 'center',
        flexDirection: 'column',
    },
    anchoMenu: {
        flex: 1,
        flexDirection: 'row',
        marginRight: 10,
        marginLeft: 10,
        marginBottom: 20,
    },
    imagen: {
        flex: 1,
    },
});