import React from 'react';
import {View, Text, Image} from 'react-native';

const Cupones1 = () => {
    return (
	    	<View style={styles.contentStyle}>

	    		<View style={styles.row}>
					<View style={[styles.box, styles.box3]}>
						<View style={styles.container}>
							<Image style={styles.imageStyle} source={require( '../img/cuponera.jpg')} />
						</View>
					</View>

					<View style={[styles.two, styles.box2]}>
						<View style={styles.container}>
							<Image style={styles.imageStyle} source={require( '../img/cuponera.jpg')} />
						</View>
					</View>
				</View>

				<View style={styles.row}>
					<View style={[styles.two, styles.box2]}>
						<Image style={styles.imageStyle} source={require( '../img/cuponera.jpg')} />
					</View>
					
					<View style={[styles.box, styles.box2]}>
						<Image style={styles.imageStyle} source={require( '../img/cuponera.jpg')} />
					</View>
				</View>

				<View style={styles.row}>
					<View style={styles.three}>
						<Image style={styles.imageStyle} source={require( '../img/cuponera.jpg')} />
					</View>
				</View>

				<View style={styles.row}>
					<View style={styles.three}>
						<Image style={styles.imageStyle} source={require( '../img/cuponera.jpg')} />
					</View>
				</View>

				<View style={styles.row}>
					<View style={[styles.two, styles.box2]}>
						<Image style={styles.imageStyle} source={require( '../img/cuponera.jpg')} />
					</View>
					
					<View style={[styles.box, styles.box2]}>
						<Image style={styles.imageStyle} source={require( '../img/cuponera.jpg')} />
					</View>
				</View>

			</View>

		
    );
};

const styles = {
	contentStyle: {
		flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'column',
	},

	row: {
	    flex: 1,
	    justifyContent: 'space-between',
	    flexDirection:'row',
	    marginTop: 6
	},

	box: {
	    flex: 1,
	    height: 120,
	    backgroundColor: '#ff00a4',
	    marginLeft: 3,
	    marginRight: 3
	},
	two: {
	    flex: 2,
	    height: 120,
	    backgroundColor: '#ff00a4',
	    marginLeft: 3,
	    marginRight: 3
	},
	three: {
		flex: 2,
	    height: 200,
	    backgroundColor: '#ff00a4',
	    marginLeft: 3,
	    marginRight: 3
	},
	box2: {
	    backgroundColor: '#ff00a4'
	},
	box3: {
	    backgroundColor: '#ff00a4'
	},
	

	container: {
		flex: 1
	},

	imageStyle: {
		flex: 1,
		width: null,
        height: null,
	}
}

export default Cupones1;