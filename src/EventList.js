/**
 * Created by Cesar on 26/06/2017.
 */
/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    ScrollView,
    View,
    FlatList
} from 'react-native';

import ArtistBox from './ArtistBox';

export default class ferias extends Component {
    render() {
        return (
            <View>
                <FlatList
                    data = {[
                        {key: 'Sandoval' , hora: 'de agosto' , fecha: 'Viernes 4', imagen: require('../img/eventos/sandoval.jpg')},
                        {key: 'Marco Antonio Solis' , hora: 'de agosto' , fecha: 'Domingo 6', imagen: require('../img/eventos/el_buki.jpg') },
                        {key: 'Cartel de Santa' , hora: 'de agosto' , fecha: 'Lunes 7', imagen: require('../img/eventos/cartel_santa.jpg') },
                        {key: 'Paty Cantú' , hora: 'de agosto' , fecha: 'Martes 8', imagen: require('../img/eventos/paty_cantu.jpg') },
                        {key: 'Los Yonics' , hora: 'de agosto' , fecha: 'Miercoles 9', imagen: require('../img/eventos/los_yonics.jpg') },
                        {key: '80'+"'"+' Sinfonico' , hora: 'de agosto' , fecha: 'Jueves 10', imagen: require('../img/eventos/80_sinfonico.jpg') },
                        {key: 'Saul el Jaguar' , hora: 'de agosto' , fecha: 'Viernes 11', imagen: require('../img/eventos/el_jaguar.jpg') },
                        {key: 'Fredy y los Villanos' , hora: 'de agosto' , fecha: 'Domingo 13', imagen: require('../img/eventos/fredy_los_villanos.jpg') },
                        {key: 'La Trakalosa' , hora: 'de agosto' , fecha: 'Lunes 14', imagen: require('../img/eventos/trakalosa.jpg') },
                        {key: 'Grupo Bryndis' , hora: 'de agosto' , fecha: 'Martes 15', imagen: require('../img/eventos/grupo_bryndis.jpg') },
                        {key: 'Inspector' , hora: 'de agosto' , fecha: 'Miercoles 16', imagen: require('../img/eventos/inspector.jpg') },
                        {key: 'Selva Negra' , hora: 'de agosto' , fecha: 'Jueves 17', imagen: require('../img/eventos/selva_negra.jpg') },
                        {key: 'Vallenato' , hora: 'de agosto' , fecha: 'Viernes 18', imagen: require('../img/eventos/vallenato.jpg') },
                        {key: 'NG' , hora: 'de agosto' , fecha: 'Sabado 19', imagen: require('../img/eventos/nuevos_generales.jpg') },
                        {key: 'Reyes de America' , hora: 'de agosto' , fecha: 'Domingo 20', imagen: require('../img/eventos/reyes_de_america.jpg') },
                        {key: 'Fidel Rueda' , hora: 'de agosto' , fecha: 'Lunes 21', imagen: require('../img/eventos/fidel_rueda.jpg') },
                        {key: 'Alfedo Rivas' , hora: 'de agosto' , fecha: 'Martes 22', imagen: require('../img/eventos/alfredo_olivas.jpg') },
                        {key: 'Cardenales de Nuevo Leon' , hora: 'de agosto' , fecha: 'Miercoles 23', imagen: require('../img/eventos/cardenales.jpg') },
                        {key: 'CD9' , hora: 'de agosto' , fecha: 'Jueves 24', imagen: require('../img/eventos/cd9.jpg') },
                        {key: 'Homenaje Norteño' , hora: 'de agosto' , fecha: 'Viernes 25', imagen: require('../img/eventos/homenaje_norteno.jpg') },
                        {key: 'Destructora de México' , hora: 'de agosto' , fecha: 'Sabado 26', imagen: require('../img/eventos/destructora.jpg') },
                    ]}
                    renderItem ={ ({item}) => <ArtistBox
                                                name = {item.key}
                                                hora = {item.hora}
                                                fecha = {item.fecha}
                                                imagen = {item.imagen}/> }

                />
            </View>

        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
});
