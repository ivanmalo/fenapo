/**
 * Created by Cesar on 27/06/2017.
 */
/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */
import React, { Component } from 'react';
import {
    AppState,
    StyleSheet,
    StatusBar,
    Image,
    CameraRoll,
    Platform,
    Text,
    TouchableHighlight,
    View,
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import PushController from './PushController';
import PushNotification from 'react-native-push-notification';
import Picker from 'react-native-image-picker';
import RNFetchBlob from 'react-native-fetch-blob';

export default class ferias extends Component {
    constructor(props){
        super(props);
        this.handleAppStateChange = this.handleAppStateChange.bind(this);
        this.state = {
            seconds: 5,
            avatarSource:''
        };
    }

    componentDidMount() {
        AppState.addEventListener('change', this.handleAppStateChange);
    }

    componentWillUnmount() {
        AppState.removeEventListener('change', this.handleAppStateChange);
    }


    handleAppStateChange(appState){
        if(appState === 'background'){
            let date = new Date(Date.now() + (this.state.seconds * 1000));
            let date2 = new Date("August 1, 2017 19:16:00");
            let notificaciones = [
                    {key: 'Sandoval' ,                  fecha: 'August 4, 2017 14:30:00' },
                    {key: 'Marco Antonio Solis'  ,      fecha: 'August 6, 2017 14:30:00' },
                    {key: 'Cartel de Santa'  ,          fecha: 'August 7, 2017 14:30:00' },
                    {key: 'Paty Cantú'  ,               fecha: 'August 8, 2017 14:30:00' },
                    {key: 'Los Yonics'  ,               fecha: 'August 9, 2017 14:30:00' },
                    {key: 'Saul el Jaguar' ,            fecha: 'August 11, 2017 14:30:00' },
                    {key: 'La Trakalosa'  ,             fecha: 'August 14, 2017 14:30:00' },
                    {key: 'Grupo Bryndis' ,             fecha: 'August 15, 2017 14:30:00' },
                    {key: 'Inspector' ,                 fecha: 'August 16, 2017 14:30:00' },
                    {key: 'Reyes de America' ,          fecha: 'August 20, 2017 14:30:00' },
                    {key: 'Fidel Rueda'  ,              fecha: 'August 21, 2017 14:30:00' },
                    {key: 'Alfredo Rivas' ,              fecha: 'August 22, 2017 14:30:00' },
                    {key: 'Cardenales de Nuevo Leon' ,  fecha: 'August 23, 2017 14:30:00' },
                    {key: 'CD9' ,                       fecha: 'August 24, 2017 14:30:00' },
                    {key: 'Homenaje Norteño' ,          fecha: 'August 25, 2017 14:30:00' },
                ];

            for(i = 0; i < notificaciones.length; i++){
                let fecha = new Date(notificaciones[i].fecha);

                if(Platform.OS === 'ios'){
                    fecha = fecha.toISOString();
                }
                if(fecha > Date.now()){

                    PushNotification.localNotificationSchedule({
                        //message: "Notificación de prueba. Programada para: "+date, // (required)
                        message: "La Feria Nacional Potosina te invita a disfrutar de "
                                + notificaciones[i].key + " el día de hoy en el teatro del pueblo",
                        date: fecha // in 60 secs
                    });
                }

            }
        }
    }


    render() {
        return (
            <View style={ styles.mainContainer }>
                <StatusBar
                    backgroundColor='#7B274D'
                    barStyle="light-content"
                />
                <View style = {styles.anchoMenu}>
                    <View style = {styles.menu}>
                        <View style = {styles.containerLogo }>
                            <Image style = {styles.imgLogo} source={ require( '../img/logoFeria.png' ) } />
                        </View>
                        
                        <View style = {styles.containerEventos }>

                            <View style = {styles.subContainerEvento}>
                                <Image style = {styles.eventos}
                                       source={ require( '../img/eventos.jpg' ) } >
                                </Image>
                                <TouchableHighlight style={ styles.containerTxt}
                                                    onPress={Actions.EventList}>
                                    <Text style = {styles.texto}>
                                        EVENTOS
                                    </Text>
                                </TouchableHighlight >
                            </View>

                            <View style = {styles.subContainerMapa}>
                                <Image style = {styles.mapa } source={ require( '../img/feria.jpg' ) } >
                                </Image>
                                <View style={ styles.containerTxt}>
                                    <Text style = {styles.texto}
                                          onPress={Actions.mapa}>
                                        MAPA
                                    </Text>
                                </View>
                            </View>

                        </View>
                        
                        <View style = {styles.containerImgSelfie} >
                            <Image style = {styles.containerSelfie } source={ require( '../img/selfieStation.jpg' ) } >
                            </Image>
                            <TouchableHighlight style={ styles.containerTxt}
                                                onPress={Actions.camara}>
                                <Text style = {styles.texto}
                                      >
                                    SELFIE STATION
                                </Text>
                            </TouchableHighlight>
                        </View>
                        <View style = {styles.containerEventos }>
                            <View style = {styles.containerCuponera }>
                                <Image style = {styles.imgCuponera } source={ require( '../img/cuponera.jpg' ) } >
                                </Image>
                                <TouchableHighlight style={ styles.containerTxt} onPress={Actions.Cuponera} >
                                    <Text style = {styles.texto} >
                                        CUPONERA
                                    </Text>
                                </TouchableHighlight>
                            </View>

                            <View style = {styles.containerGaleria }>
                                <Image style = {styles.imgGaleria } source={ require( '../img/menu_selfieo.jpg' ) } >
                                </Image>
                                <TouchableHighlight style={ styles.containerTxt} onPress={Actions.Galeria} >
                                    <Text style = {styles.texto} >
                                        GALERÍA
                                    </Text>
                                </TouchableHighlight>
                            </View>

                        </View>
                </View>
                </View>
                <PushController/>
            </View>

        );
    }
}

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#B11F61',
        flexDirection: 'column',
    },
    anchoMenu: {
        flex: 1,
        flexDirection: 'row',
        marginRight: 10,
        marginLeft: 10,
        marginBottom: 20,
    },
    menu: {
        flex: 1,
        flexDirection: 'column',
    },
    uberContainerTxt: {


    },
    containerTxt:{
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        justifyContent: 'center',
        alignItems: 'center',
    },
    texto: {
        fontSize: 25,
        color: 'white',
        fontWeight: 'bold',
    },
    containerLogo: {
        flex: 2,
        flexDirection: 'row',
        backgroundColor: 'white',
    },
    imgLogo: {
        flex: 1,
        width: null,
        height: null,
    },
    containerEventos: {
        flex: 3,
        flexDirection: 'row',
        backgroundColor: 'white',
        borderStyle: 'solid',
    },
    subContainerEvento: {
        flex: 1,
        borderRightColor: '#B11F61',
        borderRightWidth: 1,
        borderBottomWidth: 1,
        borderBottomColor: '#B11F61',
        position: 'relative',
    },
    subContainerMapa: {
        flex: 1,
        borderLeftColor: '#B11F61',
        borderLeftWidth: 1,
        borderBottomWidth: 1,
        borderBottomColor: '#B11F61',
    },
    containerImgSelfie:{
        flex: 5,
        borderTopColor: '#B11F61',
        borderTopWidth: 1,
        borderBottomWidth: 1,
        borderBottomColor: '#B11F61',
    },
    containerSelfie: {
        flex: 1,
        backgroundColor: 'skyblue',
        opacity: 0.35,
        width: null,
        height: null,
    },
    containerCuponera: {
        flex: 2,
        backgroundColor: 'white',
        borderTopColor: '#B11F61',
        borderTopWidth: 1,
    },
    imgCuponera: {
        flex: 1,
        backgroundColor: 'green',
        opacity: 0.35,
        width: null,
        height: null,
    },
    containerGaleria: {
        flex: 2,
        backgroundColor: 'white',
        borderTopColor: '#D37D00',
        borderTopWidth: 1,
    },
    imgGaleria: {
        flex: 1,
        backgroundColor: '#D37D00',
        opacity: 0.35,
        width: null,
        height: null,
    },
    eventos: {
        flex: 1,
        backgroundColor: '#B11F61',
        opacity: 0.35,
        width: null,
        height: null,
        zIndex: -1,
    },
    mapa: {
        flex: 1,
        backgroundColor: 'yellow',
        opacity: 0.35,
        width: null,
        height: null,
    },
});
