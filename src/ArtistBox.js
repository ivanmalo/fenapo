/**
 * Created by Cesar on 26/06/2017.
 */

import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
} from 'react-native';

export default class ArtistBox extends Component {
    render() {
        const  name  = this.props.name;
        const  hora  = this.props.hora;
        const  fecha  = this.props.fecha;
        const  imagen  = this.props.imagen;
        return (
            <View style = {styles.artistBox}>
                <Image style = {styles.image} source={imagen} />
                <View style = {styles.info}>
                    <View style = {styles.column}>
                        <View style = {styles.textoLugar} >
                            <Text style = {styles.name}>{ name }</Text>
                        </View>
                        <View style = {styles.fechaHora} >
                            <Text style = {styles.textoHora} > {fecha}  </Text>
                        </View>
                    </View>
                </View>
            </View>
        );}
}

const styles = StyleSheet.create({
    artistBox: {
        backgroundColor: 'white',
        flexDirection: 'row',
        margin: 5,
        shadowOpacity: .2,
        shadowOffset: {
            height: 1,
            width: -2,
        },
        elevation: 3,
    },
    image: {
        width: 120,
        height: 80,
        borderRadius: 10,
        margin: 10,
    },
    info: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
    },
    name: {
        fontSize: 18,
    },
    textoLugar: {
        alignItems: 'center',
        justifyContent: 'center',
        marginBottom: 10,
    },
    fechaHora: {
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#B04070',
        borderRadius: 10,

    },
    textoHora: {
        marginTop: 4,
        marginBottom: 4,
        color: 'white',
        fontWeight: 'bold',
    },
    row: {
        flexDirection: 'row',
        marginHorizontal: 5,
        marginTop: 10,
    },
    column: {
        flexDirection: 'column',
        margin: 10,
    },
    iconContainer: {
        flex: 1,
        alignItems: 'center'
    },
    count: {
        color: 'gray'
    }
});