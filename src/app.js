/**
 * Created by Cesar on 27/06/2017.
 */
/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    StatusBar,
    View,
    Image,
    Text,
    Dimensions,
} from 'react-native';
import {Scene, Router} from 'react-native-router-flux';

import MainMenu        from './MainMenu';
import EventList       from './EventList';
import Camara          from './Camara';
import Camara2         from './Camara00';
import ImagePreview    from './ImagePreview';
import Map             from './Map';
import Galeria          from './Galeria';
import Cuponera          from './Cuponera';

export default class ferias extends Component {

    render() {
        return (

            <Router>
                <Scene key="root">
                    <Scene key="menu" component={MainMenu} title="Menu" initial={true} hideNavBar = {true}/>
                    <Scene key="EventList" component={EventList} title="EventList"/>
                    <Scene key="camara" component={Camara} title="Camara"/>
                    <Scene key="camara2" component={Camara2} title="Camara2"/>
                    <Scene key="imagen" component={ImagePreview} title="imagen"/>
                    <Scene key="mapa" component={Map} title="mapa"/>
                    <Scene key="Galeria" component={Galeria} title="Galeria"/>
                    <Scene key="Cuponera" component={Cuponera} title="Cuponera"/>
                </Scene>
            </Router>
        );
    }
}

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#B11F61',
        flexDirection: 'column',
    },
    anchoMenu: {
        flex: 1,
        flexDirection: 'row',
        marginRight: 10,
        marginLeft: 10,
        marginBottom: 20,
    },
    menu: {
        flex: 1,
        flexDirection: 'column',
    },
    uberContainerTxt: {


    },
    containerTxt:{
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        justifyContent: 'center',
        alignItems: 'center'
    },
    texto: {
        fontSize: 25,
        color: 'white',
        fontWeight: 'bold',
    },
    containerLogo: {
        flex: 2,
        flexDirection: 'row',
        backgroundColor: 'white',
    },
    imgLogo: {
        flex: 1,
        width: null,
        height: null,
    },
    containerEventos: {
        flex: 3,
        flexDirection: 'row',
        backgroundColor: 'white',
        borderStyle: 'solid',
    },
    subContainerEvento: {
        flex: 1,
        borderRightColor: '#B11F61',
        borderRightWidth: 1,
        borderBottomWidth: 1,
        borderBottomColor: '#B11F61',
        position: 'relative',
    },
    subContainerMapa: {
        flex: 1,
        borderLeftColor: '#B11F61',
        borderLeftWidth: 1,
        borderBottomWidth: 1,
        borderBottomColor: '#B11F61',
    },
    containerImgSelfie:{
        flex: 5,
        borderTopColor: '#B11F61',
        borderTopWidth: 1,
        borderBottomWidth: 1,
        borderBottomColor: '#B11F61',
    },
    containerSelfie: {
        flex: 1,
        backgroundColor: 'skyblue',
        opacity: 0.35,
        width: null,
        height: null,
    },
    containerCuponera: {
        flex: 2,
        backgroundColor: 'white',
        borderTopColor: '#B11F61',
        borderTopWidth: 1,
    },
    imgCuponera: {
        flex: 1,
        backgroundColor: 'green',
        opacity: 0.35,
        width: null,
        height: null,
    },
    eventos: {
        flex: 1,
        backgroundColor: '#B11F61',
        opacity: 0.35,
        width: null,
        height: null,
        zIndex: -1,
    },
    mapa: {
        flex: 1,
        backgroundColor: 'yellow',
        opacity: 0.35,
        width: null,
        height: null,
    },
});

AppRegistry.registerComponent('ferias', () => ferias);