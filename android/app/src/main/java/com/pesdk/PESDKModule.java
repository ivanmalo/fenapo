package com.pesdk;


import android.support.annotation.NonNull;

import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.ferias.R;

import ly.img.android.sdk.decoder.ImageSource;
import ly.img.android.sdk.models.config.Divider;
import ly.img.android.sdk.models.config.FrameConfig;
import ly.img.android.sdk.models.config.ImageStickerConfig;
import ly.img.android.sdk.models.config.StickerCategoryConfig;
import ly.img.android.sdk.models.constant.Directory;
import ly.img.android.sdk.models.frame.CustomPatchFrameConfig;
import ly.img.android.sdk.models.frame.FrameImageGroup;
import ly.img.android.sdk.models.frame.FrameLayoutMode;
import ly.img.android.sdk.models.frame.FrameTileMode;
import ly.img.android.sdk.models.state.EditorLoadSettings;
import ly.img.android.sdk.models.state.EditorSaveSettings;
import ly.img.android.sdk.models.state.manager.SettingsList;
import ly.img.android.sdk.tools.FrameEditorTool;
import ly.img.android.sdk.tools.StickerEditorTool;
import ly.img.android.ui.activities.PhotoEditorBuilder;

public class PESDKModule extends ReactContextBaseJavaModule {
    public static int PESDK_EDITOR_RESULT = 1;
    public static final FrameTileMode REPETIDO = FrameTileMode.Repeat;
    public static final FrameTileMode COMPLETO = FrameTileMode.Stretch;
    public static final FrameLayoutMode HORIZONTAL = FrameLayoutMode.HorizontalInside;

    public PESDKModule(ReactApplicationContext reactContext) {
        super(reactContext);
    }

    @Override
    public String getName() {
        return "PESDK";
    }

    @ReactMethod
    public void present(@NonNull String image) {
        if (getCurrentActivity() != null) {

            SettingsList settingsList = new SettingsList();

            FrameConfig holy = new FrameConfig(
                    R.string.frame,
                    R.mipmap.frame_fenapo,
                    new CustomPatchFrameConfig(
                            HORIZONTAL,
                            new FrameImageGroup(
                                    ImageSource.create(R.mipmap.side_tras),
                                    COMPLETO
                            ),
                            new FrameImageGroup(
                                    ImageSource.create(R.mipmap.side_tras),
                                    COMPLETO
                            ),
                            new FrameImageGroup(
                                    ImageSource.create(R.mipmap.side_tras),
                                    COMPLETO
                            ),
                            new FrameImageGroup(
                                    ImageSource.create(R.mipmap.frame_fenapo),
                                    COMPLETO
                            )
                    )
            );

            settingsList.getSettingsModel(EditorLoadSettings.class)
                .setImageSourcePath(image, true)
                .getSettingsModel(EditorSaveSettings.class)
                .setExportDir(Directory.DCIM, "fenapo")
                .setExportPrefix("fenapo_")
                .setSavePolicy(
                        EditorSaveSettings.SavePolicy.RETURN_ALWAYS_ONLY_OUTPUT
                );
            settingsList.getConfig()
                    .setTools(
                            new StickerEditorTool(R.string.stiker_sm, R.drawable.imgly_icon_tool_sticker),
                            new Divider(),
                            new FrameEditorTool(R.string.marcos_sm, R.drawable.imgly_icon_option_frame_none_normal)
                    )
                    .setStickerLists (
                            new StickerCategoryConfig(
                                    "SM Stickers",
                                    ImageSource.create(R.mipmap.sticker_fenapo),
                                    new ImageStickerConfig(
                                            "corona",
                                            ImageSource.create(R.mipmap.sticker_fenapo),
                                            ImageSource.create(R.mipmap.sticker_fenapo)
                                    ),
                                    new ImageStickerConfig(
                                            "Cawama",
                                            ImageSource.create(R.mipmap.sticker_logo),
                                            ImageSource.create(R.mipmap.sticker_logo)
                                    )
                            )
                    ).setFrames(
                            holy
                    );

            new PhotoEditorBuilder(getCurrentActivity())
                    .setSettingsList(settingsList)
                    .startActivityForResult(getCurrentActivity(), PESDK_EDITOR_RESULT);
        }
    }
}